class Ingredient:
    def __init__(self, name: str = "", price: int = 0, description: str = ""):
        self.Name = name
        self.Price = price
        self.Description = description

    def __lt__(self, other):
        return (self.Name < other.Name)


class Pizza:
    def __init__(self, name: str = ""):
        self.Name = name
        self.Ingredients= []

    def addIngredient(self, ingredient: Ingredient):
        self.Ingredients.append(ingredient)

    def numIngredients(self) -> int:
        return len(self.Ingredients)

    def computePrice(self) -> int:
        price = 0
        for ingredient in self.Ingredients:
            price += ingredient.Price
        return price


class Order:
    def __init__(self):
        self.number :int = 0
        self.Pizzas = []

    def getPizza(self, position: int) -> Pizza:
        index = position-1
        return self.Pizzas[index]

    def initializeOrder(self, numPizzas: int):
        self.Pizzas = []*numPizzas

    def addPizza(self, pizza: Pizza):
        self.Pizzas.append(pizza)

    def numPizzas(self) -> int:
        return len(self.Pizzas)

    def computeTotal(self) -> int:
        price = 0
        for pizza in self.Pizzas:
            price += pizza.computePrice()
        return price


class Pizzeria:
    def __init__(self):
        self.ListIngredients = []
        self.ListPizzas = []
        self.ListOrders = []

    def addIngredient(self, name: str, description: str, price: int):
        # Checking if already inserted
        for ingredient in self.ListIngredients:
            if ingredient.Name is name:
                raise Exception("Ingredient already inserted")

        newIngredient = Ingredient()
        newIngredient.Name = name
        newIngredient.Description = description
        newIngredient.Price = price
        self.ListIngredients.append(newIngredient)
        # Alphabetical order
        self.ListIngredients.sort()


    def findIngredient(self, name: str) -> Ingredient:
        for ingredient in self.ListIngredients:
            if ingredient.Name is name:
                return ingredient
        raise Exception("Ingredient not found")

    def addPizza(self, name: str, ingredients: []):
        # Checking if already inserted
        for pizza in self.ListPizzas:
            if pizza.Name is name:
                raise Exception("Pizza already inserted")

        newPizza = Pizza(name)
        newIngredient = Ingredient()
        for one in ingredients:
            newIngredient = self.findIngredient(one)
            newPizza.addIngredient(newIngredient)
        self.ListPizzas.append(newPizza)



    def findPizza(self, name: str) -> Pizza:
        for pizza in self.ListPizzas:
            if pizza.Name == name:
                return pizza
        raise Exception("Pizza not found")

    def createOrder(self, pizzas: []) -> int:
        #Checking if empty
        if len(pizzas) == 0:
            raise Exception("Empty order")

        newOrder = Order()
        newOrder.number = 1000 + len(self.ListOrders)
        numPizzas = len(pizzas)
        newOrder.initializeOrder(numPizzas)
        for pizza in pizzas:
            newPizza = self.findPizza(pizza)
            newOrder.addPizza(newPizza)
        self.ListOrders.append(newOrder)

        return newOrder.number

    def findOrder(self, numOrder: int) -> Order:
        for order in self.ListOrders:
            if order.number == numOrder:
                return order
        raise Exception("Order not found")

    def getReceipt(self, numOrder: int) -> str:
        order = self.findOrder(numOrder)
        receipt = ""
        for pizza in order.Pizzas:
            receipt += "- " + pizza.Name + ", " + str(pizza.computePrice()) + " euro" + "\n"
        receipt += "  TOTAL: " + str(order.computeTotal()) + " euro" + "\n"
        return  receipt

    def listIngredients(self) -> str:
        list = ""
        for ingredient in self.ListIngredients:
            list += ingredient.Name + " - '" + ingredient.Description + "': " + str(ingredient.Price) + " euro" + '\n'
        return list

    def menu(self) -> str:
        list = ""
        for pizza in self.ListPizzas:
            list += pizza.Name + " (" + str(pizza.numIngredients()) + " ingredients): " + str(pizza.computePrice()) \
                    + " euro" + '\n'
        return list
