#ifndef BUS_H
#define BUS_H

#include <iostream>

using namespace std;

namespace BusLibrary {
  class Bus {
    public:
      int Id;
      int FuelCost;
  };

  class BusStop {
    public:
      int Id;
      int Latitude;
      int Longitude;
      string Name;
  };

  class Street {
    public:
      int Id;
      int TravelTime;
  };

  class Route {
    public:
      int Id;
      int NumberStreets;
  };


  class IBusStation {
    public:
      virtual void Load() = 0;
      virtual int NumberBuses() const = 0;
      virtual const Bus& GetBus(const int& idBus) const = 0;
  };

  class IMapData {
    public:
      virtual void Load() = 0;
      virtual int NumberRoutes() const = 0;
      virtual int NumberStreets() const = 0;
      virtual int NumberBusStops() const = 0;
      virtual const Street& GetRouteStreet(const int& idRoute, const int& streetPosition) const = 0;
      virtual const Route& GetRoute(const int& idRoute) const = 0;
      virtual const Street& GetStreet(const int& idStreet) const = 0;
      virtual const BusStop& GetStreetFrom(const int& idStreet) const = 0;
      virtual const BusStop& GetStreetTo(const int& idStreet) const = 0;
      virtual const BusStop& GetBusStop(const int& idBusStop) const = 0;
  };

  class IRoutePlanner {
    public:
      virtual int ComputeRouteTravelTime(const int& idRoute) const = 0;
      virtual int ComputeRouteCost(const int& idBus, const int& idRoute) const = 0;
  };

  class IMapViewer {
    public:
      virtual string ViewRoute(const int& idRoute) const = 0;
      virtual string ViewStreet(const int& idStreet) const = 0;
      virtual string ViewBusStop(const int& idBusStop) const = 0;
  };
}

#endif // BUS_H
